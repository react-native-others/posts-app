import axios from "axios";
import {AsyncStorage} from 'react-native';

const posts_storage_name = 'posts';

class PostsService {
    getPostsFromStorage() {
        return new Promise((resolve, reject) => {
            AsyncStorage.getItem(posts_storage_name).then(data => {
                if (data !== null) {
                    resolve(JSON.parse(data))
                }
                resolve([]);
            });
        });
    }

    getPosts() {
        return new Promise((resolve, reject) => {
            const promises = [
                axios.get('https://jsonplaceholder.typicode.com/posts'),
                axios.get('https://jsonplaceholder.typicode.com/users')
            ];

            Promise.all(promises).then(responses => {
                const posts = responses[0].data;
                const users = responses[1].data;
                const response = {
                    posts: posts.map(p => {
                        return {
                            ...p,
                            user: users.find(u => u.id == p.userId)
                        }
                    }),
                    users
                }
                AsyncStorage.setItem(posts_storage_name, JSON.stringify(response.posts)).then(() => {
                    resolve(response);
                });
            });
        });
    }
}

export const postService = new PostsService();