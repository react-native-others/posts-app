import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

export default class Post extends Component {
    render() {
        const { post } = this.props;
        return (
            <View style={styles.container}>
                <Text style={styles.postTitle}>{post.title}</Text>
                <Text style={styles.postAuthor}>By { post.user.name }</Text>
                <Text style={styles.post}>
                    { post.body }
                </Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderBottomWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderTopWidth: 1,
        borderColor: '#000',
        marginLeft: 30,
        marginRight: 30,
        paddingTop: 20,
        marginBottom: 40,
    },
    postTitle: {
        textAlign: 'center',
        fontSize: 18,
        paddingBottom: 6,
    },
    postAuthor: {
        textAlign: 'right',
        paddingBottom: 20,
        paddingRight: 20
    },
    post: {
        textAlign: 'left',
        paddingLeft: 30,
        paddingRight: 30,
        paddingBottom: 15,
    }
});
