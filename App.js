import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, FlatList, SafeAreaView} from 'react-native';
import Post from './shared/components/Post';
import {postService} from './shared/services/PostsService';

type Props = {};
export default class App extends Component<Props> {
  componentWillMount() {
    postService.getPosts().then(data => {
      const { posts } = data;
      this.setState({
        posts
      });
    });
  }
  state = {
    posts: []
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <FlatList 
          data={ this.state.posts }
          renderItem={({item}) => 
              <Post 
                post={item}
              />
          }
          keyExtractor={((item, index) => index.toString())}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
